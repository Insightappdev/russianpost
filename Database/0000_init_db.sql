SET NAMES 'utf8' COLLATE 'utf8_unicode_ci' \p;

CREATE DATABASE IF NOT EXISTS db_russian_post CHARACTER SET='utf8' COLLATE='utf8_unicode_ci' \p;

CREATE USER IF NOT EXISTS 'rp_admin'@'%' IDENTIFIED BY 'rp_admin_pwd' \p;
GRANT ALL ON db_russian_post.* TO 'rp_admin'@'%' WITH GRANT OPTION \p;
GRANT GRANT OPTION ON db_russian_post.* TO 'rp_admin'@'%' WITH GRANT OPTION \p;