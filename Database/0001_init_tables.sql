CREATE TABLE `Request` (
      `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор запроса'
    , `Text` text NOT NULL COLLATE utf8_unicode_ci COMMENT 'Сообщение'
    , `IsSent` bit(1) NOT NULL DEFAULT 0 COMMENT 'Отправлено ли'
    , `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания'
    , PRIMARY KEY (`Id`)
) ENGINE=InnoDb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `Message` (
      `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор сообщения'
    , `Text` text NOT NULL COMMENT 'Сообщение'
    , `Ip` varchar(15) NOT NULL COLLATE utf8_unicode_ci COMMENT 'Адрес отправителя'
    , `Date` timestamp NOT NULL COMMENT 'Дата получения'
    , PRIMARY KEY (`Id`)
) ENGINE=InnoDb DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
