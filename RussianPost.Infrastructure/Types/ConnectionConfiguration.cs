using System;

namespace RussianPost.Infrastructure.Types
{
    /// <summary>
    /// Конфигурация подключения к бд
    /// </summary>
    public sealed class ConnectionConfiguration
    {
        private string _connectionString;

        public ConnectionConfiguration()
        {
        }

        public string ConnectionString
        {
            get => _connectionString;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException(nameof(value));

                _connectionString = value;
            }
        }
    }
}