using System;

namespace RussianPost.Infrastructure.Types
{
    public sealed class DispatcherConfiguration
    {
        public TimeSpan Interval { get; set; } = TimeSpan.FromSeconds(5);
        
        public TimeSpan ExceptionTimeout { get; set; } = TimeSpan.FromSeconds(5);
    }
}