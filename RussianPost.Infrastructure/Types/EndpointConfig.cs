using System;

namespace RussianPost.Infrastructure.Types
{
    /// <summary>
    /// Конфигурация точки подключения
    /// </summary>
    public sealed class EndpointConfig
    {
        private string _address;

        public EndpointConfig()
        {
        }

        public string Address
        {
            get => _address;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException(nameof(value));

                _address = value;
            }
        }
        
        public int Port { get; set; }
        
        public string Path { get; set; }
    }
}