using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace RussianPost.Infrastructure.Json
{
    public sealed class PrivateFieldsJsonContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            if (!property.Writable)
            {
                var info = member as PropertyInfo;
                if (info != null)
                {
                    property.Writable = info.GetSetMethod(true) != null;
                }
            }

            return property;
        }
    }

    public static class JSerializer
    {
        private static readonly JsonSerializerSettings _settings;

        static JSerializer()
        {
            _settings = new JsonSerializerSettings
            {
                ContractResolver = new PrivateFieldsJsonContractResolver()
            };
        }

        public static void RegisterConverter(JsonConverter converter)
        {
            if (converter == null)
                throw new ArgumentNullException(nameof(converter));

            _settings.Converters.Add(converter);
        }

        public static string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, _settings);
        }

        public static TObject Deserialize<TObject>(string json)
        {
            return JsonConvert.DeserializeObject<TObject>(json, _settings);
        }

        public static object Deserialize(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type, _settings);
        }
    }
}