﻿using System;

namespace RussianPost.Domain.Types
{
    public sealed class Message
    {
        public string Text { get; set; }
        
        public string Ip { get; set; }
        
        public DateTimeOffset Date { get; set; }

        public override string ToString()
        {
            return $"{Date:dd/MM/yyyy HH:mm:ss} ({Ip}): {Text}";
        }
    }
}