using System;

namespace RussianPost.Domain.Types
{
    public sealed class Request
    {
        private Request()
        {
            CreatedAt = DateTimeOffset.Now;
        }

        public Request(string text) : this()
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text));

            Text = text;
        }

        public ulong Id { get; set; }

        public string Text { get; private set; }

        public bool IsSent { get; private set; }
        
        public DateTimeOffset CreatedAt { get; private set; }
    }
}