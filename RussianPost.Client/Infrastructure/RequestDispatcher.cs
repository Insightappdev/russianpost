using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using RussianPost.Client.Data;
using RussianPost.Client.Services;
using RussianPost.Infrastructure.Types;
using StructureMap;

namespace RussianPost.Client.Infrastructure
{
    public sealed class RequestDispatcher : IHostedService
    {
        private readonly IContainer _container;
        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly DispatcherConfiguration _configuration;

        public RequestDispatcher(IContainer container)
        {
            if (container == null)
                throw new ArgumentNullException(nameof(container));

            var configuration = container.GetInstance<DispatcherConfiguration>();
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _container = container;
            _configuration = configuration;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async Task StartAsync(CancellationToken token = default)
        {
            CancellationTokenSource.CreateLinkedTokenSource(_cancellationTokenSource.Token, token);
            try
            {
                while (!_cancellationTokenSource.IsCancellationRequested)
                {
                    await Task.WhenAll(RetrySendRequests(),
                        Task.Delay((int) _configuration.Interval.TotalMilliseconds, token));
                }
            }
            catch (TaskCanceledException)
            {
                throw;
            }
            catch
            {
                await Task.Delay((int) _configuration.ExceptionTimeout.TotalMilliseconds, CancellationToken.None);
            }
        }

        public Task StopAsync(CancellationToken token = default)
        {
            _cancellationTokenSource.Cancel();
            return Task.CompletedTask;
        }

        private async Task RetrySendRequests()
        {
            using (var container = _container.GetNestedContainer())
            {
                var dataService = container
                    .GetInstance<IClientRequestDataService>();
                var clientService = container
                    .GetInstance<IClientService>();

                if (dataService == null)
                    throw new ArgumentNullException(nameof(dataService));

                if (clientService == null)
                    throw new ArgumentNullException(nameof(clientService));

                var unsentRequests = await dataService.GetUnsentRequests(CancellationToken.None);
                foreach (var request in unsentRequests)
                {
                    await clientService.SendRequest(request);
                }
            }
        }
    }
}