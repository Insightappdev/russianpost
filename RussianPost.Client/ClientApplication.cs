﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RussianPost.Client.Data;
using RussianPost.Client.Data.Implementation;
using RussianPost.Client.Infrastructure;
using RussianPost.Client.Services;
using RussianPost.Client.Services.Implementation;
using RussianPost.Domain.Types;
using RussianPost.Infrastructure.Types;
using StructureMap;

namespace RussianPost.Client
{
    public sealed class ClientApplication : IDisposable
    {
        private readonly Lazy<ClientConfiguration> _configuration =
            new Lazy<ClientConfiguration>(() =>
            {
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

                var clientConfiguration = new ClientConfiguration();
                configuration.Bind(clientConfiguration);

                return clientConfiguration;
            }, true);

        private readonly Container _container;

        public ClientApplication()
        {
            _container = BuildContainer(_configuration.Value);
            ConfigureDispatcher();
            StartDispatcher().ConfigureAwait(true);
        }

        public async Task SendRequest(Request request, CancellationToken token = default)
        {
            var clientService = _container.GetInstance<IClientService>();
            await clientService
                .AddRequest(request, token);

            await clientService
                .SendRequest(request, CancellationToken.None);
        }

        private static Container BuildContainer(ClientConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            var registry = new Registry();

            registry.For<IClientService>()
                .Use<ClientService>()
                .Transient();

            registry.For<IClientRequestDataService>()
                .Use<ClientRequestDataService>()
                .Transient();

            registry.For<EndpointConfig>()
                .Use(configuration.ServerConfiguration)
                .Transient();

            registry.For<ConnectionConfiguration>()
                .Use(configuration.ConnectionConfiguration)
                .Transient();

            registry.For<DispatcherConfiguration>()
                .Use(configuration.DispatcherConfiguration);

            return new Container(registry);
        }

        private void ConfigureDispatcher()
        {
            if (_container == null)
                throw new ArgumentNullException(nameof(_container));

            var host = new HostBuilder()
                .ConfigureContainer<ServiceCollection>(services => { services.AddSingleton<IContainer>(_container); })
                .ConfigureServices((context, services) => { services.AddHostedService<RequestDispatcher>(); })
                .Build();

            _container.Configure(cfg =>
            {
                cfg.For<IHost>()
                    .Use(host)
                    .Singleton();
            });
        }

        private async Task StartDispatcher()
        {
            if (_container == null)
                throw new ArgumentNullException(nameof(_container));

            await _container
                .GetInstance<IHost>()
                .StartAsync();
        }

        public void Dispose()
        {
            _container?.Dispose();
        }
    }
}