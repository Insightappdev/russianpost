﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MySql.Data.MySqlClient;
using RussianPost.Domain.Types;
using RussianPost.Infrastructure.Data;
using RussianPost.Infrastructure.Types;

namespace RussianPost.Client.Data.Implementation
{
    public class ClientRequestDataService : IClientRequestDataService
    {
        static ClientRequestDataService()
        {
            SqlMapper.AddTypeHandler(new DateTimeOffsetHandler());
        }
        
        private readonly ConnectionConfiguration _config;

        public ClientRequestDataService(ConnectionConfiguration config)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            _config = config;
        }

        public async Task<Request> AddRequest(Request request, CancellationToken token = default)
        {
            using (var connection = new MySqlConnection(_config.ConnectionString))
            {
                var id = await connection.ExecuteScalarAsync<ulong>(
                    new CommandDefinition(
                        Queries.InsertRequest, new
                        {
                            request.Text,
                            request.IsSent,
                            request.CreatedAt
                        }, cancellationToken: token));

                request.Id = id;

                return request;
            }
        }

        public async Task<IReadOnlyCollection<Request>> GetUnsentRequests(CancellationToken token = default)
        {
            using (var connection = new MySqlConnection(_config.ConnectionString))
            {
                var requests = await connection.QueryAsync<Request>(
                    new CommandDefinition(
                        Queries.SelectUnsentRequests, new
                        {
                            From = DateTimeOffset.UtcNow - TimeSpan.FromMinutes(1)
                        }, cancellationToken: token));

                return requests.ToList();
            }
        }

        public async Task MarkRequestAsSent(ulong id, CancellationToken token = default)
        {
            using (var connection = new MySqlConnection(_config.ConnectionString))
            {
                await connection.ExecuteAsync(
                    new CommandDefinition(Queries.MarkRequestAsSent, new
                    {
                        Id = id
                    }, cancellationToken: token));
            }
        }
    }
}