namespace RussianPost.Client.Data.Implementation
{
    internal static class Queries
    {
        public const string InsertRequest = @"
INSERT INTO request (Text, IsSent, CreatedAt)
VALUES (@Text, @IsSent, @CreatedAt);

SELECT LAST_INSERT_ID()
;";

        public const string SelectUnsentRequests = @"
SELECT *
FROM request
    WHERE IsSent = 0 
    AND CreatedAt < @From;
;";

        public const string MarkRequestAsSent = @"
UPDATE request
SET IsSent = 1
    WHERE Id = @Id
;";
    }
}