using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RussianPost.Domain.Types;

namespace RussianPost.Client.Data
{
    public interface IClientRequestDataService
    {
        Task<Request> AddRequest(Request request, CancellationToken token = default);

        Task<IReadOnlyCollection<Request>> GetUnsentRequests(CancellationToken token = default);

        Task MarkRequestAsSent(ulong id, CancellationToken token = default);
    }
}