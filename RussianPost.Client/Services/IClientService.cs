using System.Threading;
using System.Threading.Tasks;
using RussianPost.Domain.Types;

namespace RussianPost.Client.Services
{
    public interface IClientService
    {
        Task<Request> AddRequest(Request request, CancellationToken token = default);
        Task SendRequest(Request request, CancellationToken token = default);
    }
}