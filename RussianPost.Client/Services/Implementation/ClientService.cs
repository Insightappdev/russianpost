using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MimeMapping;
using RussianPost.Client.Data;
using RussianPost.Domain.Types;
using RussianPost.Infrastructure.Json;
using RussianPost.Infrastructure.Types;

namespace RussianPost.Client.Services.Implementation
{
    public class ClientService : IClientService
    {
        private readonly EndpointConfig _config;
        private readonly IClientRequestDataService _dataService;

        public ClientService(EndpointConfig config, IClientRequestDataService dataService)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            if (dataService == null)
                throw new ArgumentNullException(nameof(dataService));

            _config = config;
            _dataService = dataService;
        }

        public Task<Request> AddRequest(Request request, CancellationToken token = default)
        {
            return _dataService.AddRequest(request, token);
        }

        public async Task SendRequest(Request request, CancellationToken token = default)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var body = JSerializer.Serialize(request);
                    var content = new StringContent(body, Encoding.UTF8, KnownMimeTypes.Json);
                    var uri = new UriBuilder($"{_config.Address}:{_config.Port}/{_config.Path}/").Uri;

                    var response = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Post, uri)
                    {
                        Content = content
                    }, token);

                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception($"RequestId: {request.Id}");
                }

                await _dataService.MarkRequestAsSent(request.Id, CancellationToken.None);
            }
            catch
            {
                Console.WriteLine($"Request with id: {request.Id} was not send");
            }
        }
    }
}