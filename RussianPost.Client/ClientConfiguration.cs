using RussianPost.Infrastructure.Types;

namespace RussianPost.Client
{
    public sealed class ClientConfiguration
    {
        public ConnectionConfiguration ConnectionConfiguration { get; set; }

        public EndpointConfig ServerConfiguration { get; set; }

        public DispatcherConfiguration DispatcherConfiguration { get; set; }
    }
}