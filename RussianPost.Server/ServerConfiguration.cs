using RussianPost.Infrastructure.Types;
using RussianPost.Server.Infrastructure;

namespace RussianPost.Server
{
    public class ServerConfiguration
    {
        public ConnectionConfiguration ConnectionConfiguration { get; set; }

        public WebServerConfiguration WebServerConfiguration { get; set; }
    }
}