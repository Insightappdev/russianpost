using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RussianPost.Domain.Types;
using RussianPost.Infrastructure.Types;
using RussianPost.Server.Data;
using RussianPost.Server.Data.Implementation;
using RussianPost.Server.Infrastructure;
using RussianPost.Server.Services;
using RussianPost.Server.Services.Implementation;
using StructureMap;

namespace RussianPost.Server
{
    public sealed class ServerApplication : IDisposable
    {
        private readonly Lazy<ServerConfiguration> _configuration =
            new Lazy<ServerConfiguration>(() =>
            {
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();

                var serverConfiguration = new ServerConfiguration();
                configuration.Bind(serverConfiguration);

                return serverConfiguration;
            }, true);

        private readonly Container _container;

        public ServerApplication()
        {
            _container = BuildContainer(_configuration.Value);
            ConfigureServer();
            StartWebServer().ConfigureAwait(true);
        }

        private static Container BuildContainer(ServerConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            
            var registry = new Registry();

            registry.For<IServerService>()
                .Use<ServerService>()
                .Transient();

            registry.For<IServerMessageDataService>()
                .Use<ServerMessageDataService>()
                .Transient();

            registry.For<ConnectionConfiguration>()
                .Use(configuration.ConnectionConfiguration)
                .Transient();

            registry.For<WebServerConfiguration>()
                .Use(configuration.WebServerConfiguration)
                .Transient();

            return new Container(registry);
        }

        public Task<IReadOnlyCollection<Message>> GetMessages(CancellationToken token = default)
        {
            return _container
                .GetInstance<IServerService>()
                .GetMessages(token);
        }

        private void ConfigureServer()
        {
            if (_container == null)
                throw new ArgumentNullException(nameof(_container));

            var host = new HostBuilder()
                .ConfigureContainer<ServiceCollection>(services => { services.AddSingleton<IContainer>(_container); })
                .ConfigureServices((context, services) => { services.AddHostedService<WebServerHost>(); })
                .Build();

            _container.Configure(cfg =>
            {
                cfg.For<IHost>()
                    .Use(host)
                    .Singleton();
            });
        }

        private async Task StartWebServer()
        {
            await _container
                .GetInstance<IHost>()
                .StartAsync();
        }

        public void Dispose()
        {
            _container?.Dispose();
        }
    }
}