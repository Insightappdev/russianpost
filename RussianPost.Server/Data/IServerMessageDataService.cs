using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RussianPost.Domain.Types;

namespace RussianPost.Server.Data
{
    public interface IServerMessageDataService
    {
        Task AddMessage(Message message, CancellationToken token = default);

        Task<IReadOnlyCollection<Message>> GetMessages(CancellationToken token = default);
    }
}