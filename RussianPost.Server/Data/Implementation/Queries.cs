namespace RussianPost.Server.Data.Implementation
{
    internal static class Queries
    {
        public const string InsertMessage = @"
INSERT INTO message (Text, Ip, Date)
VALUES (@Text, @Ip, @Date)
;";

        public const string SelectMessages = @"
SELECT *
FROM message;
;";
    }
}