using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using MySql.Data.MySqlClient;
using RussianPost.Domain.Types;
using RussianPost.Infrastructure.Data;
using RussianPost.Infrastructure.Types;

namespace RussianPost.Server.Data.Implementation
{
    public class ServerMessageDataService : IServerMessageDataService
    {
        static ServerMessageDataService()
        {
            SqlMapper.AddTypeHandler(new DateTimeOffsetHandler());
        }

        private readonly ConnectionConfiguration _config;

        public ServerMessageDataService(ConnectionConfiguration config)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            _config = config;
        }

        public async Task AddMessage(Message message, CancellationToken token = default)
        {
            using (var connection = new MySqlConnection(_config.ConnectionString))
            {
                await connection.ExecuteAsync(
                    new CommandDefinition(Queries.InsertMessage, new
                    {
                        message.Text,
                        message.Date,
                        message.Ip
                    }, cancellationToken: token));
            }
        }

        public async Task<IReadOnlyCollection<Message>> GetMessages(CancellationToken token = default)
        {
            using (var connection = new MySqlConnection(_config.ConnectionString))
            {
                var messages = await connection.QueryAsync<Message>(
                    new CommandDefinition(Queries.SelectMessages, cancellationToken: token));

                return messages.ToList();
            }
        }
    }
}