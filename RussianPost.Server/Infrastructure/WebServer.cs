using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RussianPost.Domain.Types;
using RussianPost.Infrastructure.Json;

namespace RussianPost.Server.Infrastructure
{
    /// <summary>
    /// Simple web server
    /// <!-- https://www.technical-recipes.com/2016/creating-a-web-server-in-c/ -->
    /// </summary>
    public sealed class WebServer : IDisposable
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<Message, CancellationToken, Task> _method;

        public WebServer(IReadOnlyCollection<string> prefixes, Func<Message, CancellationToken, Task> method)
        {
            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");
            }

            if (prefixes == null || prefixes.Count == 0)
            {
                throw new ArgumentException("URI prefixes are required");
            }

            if (method == null)
            {
                throw new ArgumentException("Method required");
            }

            foreach (var s in prefixes)
            {
                _listener.Prefixes.Add(s);
            }

            _method = method;
            _listener.Start();
        }

        public WebServer(Func<Message, CancellationToken, Task> method, params string[] prefixes)
            : this(prefixes, method)
        {
        }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (_listener.IsListening)
                {
                    ThreadPool.QueueUserWorkItem(async c =>
                    {
                        var ctx = c as HttpListenerContext;
                        if (ctx == null)
                        {
                            return;
                        }

                        try
                        {
                            var buffer = new byte[(int) ctx.Request.ContentLength64];
                            await ctx.Request.InputStream.ReadAsync(buffer);
                            var input = JSerializer.Deserialize<Request>(Encoding.UTF8.GetString(buffer));

                            var message = new Message
                            {
                                Text = input.Text,
                                Ip = ctx.Request.RemoteEndPoint.Address.ToString(),
                                Date = DateTimeOffset.Now
                            };

                            await _method(message, CancellationToken.None);
                            ctx.Response.StatusCode = 200;
                        }
                        catch
                        {
                            ctx.Response.StatusCode = 500;
                        }
                        finally
                        {
                            ctx?.Response.OutputStream.Close();
                        }
                    }, _listener.GetContext());
                }
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }

        public void Dispose()
        {
            ((IDisposable) _listener)?.Dispose();
        }
    }
}