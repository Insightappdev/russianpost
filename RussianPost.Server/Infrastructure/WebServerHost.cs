using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using RussianPost.Domain.Types;
using RussianPost.Server.Services;
using StructureMap;

namespace RussianPost.Server.Infrastructure
{
    public sealed class WebServerHost : IHostedService
    {
        private readonly IContainer _container;
        private readonly WebServerConfiguration _config;
        private readonly WebServer _server;

        public WebServerHost(IContainer container)
        {
            if (container == null)
                throw new ArgumentNullException(nameof(container));

            var config = container.GetInstance<WebServerConfiguration>();
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            _container = container;
            _config = config;
            _server = new WebServer(ReceiveMessage, _config.Address);
        }

        public Task StartAsync(CancellationToken token)
        {
            _server.Run();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken token)
        {
            _server.Stop();
            return Task.CompletedTask;
        }

        private async Task ReceiveMessage(Message message, CancellationToken token = default)
        {
            using (var nested = _container.GetNestedContainer())
            {
                var serverService = nested.GetInstance<IServerService>();
                if (serverService == null)
                    throw new ArgumentNullException(nameof(serverService));

                await serverService.ReceiveMessage(message, token);
            }
        }
    }
}