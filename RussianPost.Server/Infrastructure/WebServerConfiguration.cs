namespace RussianPost.Server.Infrastructure
{
    public sealed class WebServerConfiguration
    {
        public string Address { get; set; }
    }
}