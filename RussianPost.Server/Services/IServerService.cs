using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RussianPost.Domain.Types;

namespace RussianPost.Server.Services
{
    public interface IServerService
    {
        Task ReceiveMessage(Message message, CancellationToken token = default);

        Task<IReadOnlyCollection<Message>> GetMessages(CancellationToken token = default);
    }
}