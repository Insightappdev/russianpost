using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using RussianPost.Domain.Types;
using RussianPost.Server.Data;

namespace RussianPost.Server.Services.Implementation
{
    public sealed class ServerService : IServerService
    {
        private readonly IServerMessageDataService _dataService;

        public ServerService(IServerMessageDataService dataService)
        {
            if (dataService == null)
                throw new ArgumentNullException(nameof(dataService));
            
            _dataService = dataService;
        }
        
        public Task ReceiveMessage(Message message, CancellationToken token = default)
        {
            return _dataService.AddMessage(message, token);
        }

        public Task<IReadOnlyCollection<Message>> GetMessages(CancellationToken token = default)
        {
            return _dataService.GetMessages(token);
        }
    }
}