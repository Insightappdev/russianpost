﻿using System;
using System.Threading.Tasks;
using RussianPost.Client;
using RussianPost.Domain.Types;

namespace ClientSrv
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using (var client = new ClientApplication())
            {
                while (true)
                {
                    var str = Console.ReadLine();
                    if (string.IsNullOrEmpty(str))
                    {
                        Console.WriteLine("String is empty!");
                        continue;
                    }

                    await client.SendRequest(new Request(str));
                }
            }
        }
    }
}