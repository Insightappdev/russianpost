﻿using System;
using System.Linq;
using System.Threading.Tasks;
using RussianPost.Server;

namespace ServerSrv
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using (var server = new ServerApplication())
            {
                while (true)
                {
                    var str = Console.ReadLine();
                    if (string.IsNullOrEmpty(str))
                    {
                        Console.WriteLine("String is empty!");
                        continue;
                    }

                    if (str.Equals("print", StringComparison.OrdinalIgnoreCase))
                    {
                        var messages = (await server.GetMessages()).OrderBy(x => x.Date);
                        foreach (var message in messages)
                        {
                            Console.WriteLine(message);
                        }
                    }
                }
            }
        }
    }
}